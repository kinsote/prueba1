/*  -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------


    * * * * * * * * * * * * * * * *
    *  1. C A L C U L A D O R A   *
    * * * * * * * * * * * * * * * *
    
    Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones. 

        - El programa debe recibir dos números (n1, n2).

        - Debe existir una variable que permita seleccionar de alguna forma el tipo de operación (suma, resta,
          multiplicación o división).

        - Opcional: agrega una operación que permita elevar n1 a la potencia n2.
    -----------------------------------------------------------------------------------------------------------------------
*/

function calculadora(operacion, n1, n2) {

    if (operacion === 'suma') {
        return (n1 + n2);
    } else if (operacion === 'resta') {
        return (n1 - n2);
    } else if (operacion === 'division') {
        return (n1 / n2);
    } else if (operacion === 'multiplicacion') {
        return (n1 * n2);
    } else if (operacion === 'elevado') {
        return (Math.pow(n1, n2));

    }
}

console.log(calculadora('multiplicacion', 5, 3));



/*

----------------------------------------------------------------------------------------------------------------------


/* * * * * * * * * * * * * * * * * * * * 
*  2. D A D O   E L E C T R Ó N I C O   *
* * * * * * * * * * * * * * * * * * * * *

Simula el uso de un dado electrónico cuyos valores al azar irán del 1 al 6.

    - Crea una variable "totalScore" en la que se irá¡ almacenando la puntuación total tras cada una de las tiradas.

    - Una vez alcanzados los 50 puntos el programa se detendrá¡ y se mostrará¡ un mensaje que indique el fin de la partida.

    - Debes mostrar por pantalla los distintos valores que nos devuelva el dado (números del 1 al 6) así­ como el valor de la
      variable "totalScore" tras cada una de las tiradas.


-----------------------------------------------------------------------------------------------------------------------
*/

let totalScore = 0;

while (totalScore <= 44) {

    let tirada = Math.floor(Math.random() * 7);

    totalScore = totalScore + tirada;
    console.log('dado: ', tirada, '   total tiradas ', totalScore);
}
console.log('la partida ha terminado');




/*
----------------------------------------------------------------------------------------------------------------------


* * * * * * * * * * * * * * * * * * * * * * *
*  3. ASINCRONIA                            *
* * * * * * * * * * * * * * * * * * * * * * *

En este ejercicio se comprobará¡ la competencia de los alumnos en el concepto de asincroní­a.
Se proporcionan 3 archivos  csv separados por comas y se deberán bajar asíncronamente (promises)

A la salida se juntarán los registros de los 3 archivos en un array que será¡ el parámetro de entrada
de la funcion findIPbyName(array, name ,surname) que buscará una entrada en el array y devolverá la IP correspondiente

Una vez hallada la IP ha de mostrarse por pantalla

para llamar a la función utilizad el nombre Cari Wederell
-----------------------------------------------------------------------------------------------------------------------
*/
IP_POSITION = 5;

const fsPromises = require('fs').promises;

function findIPbyName(datos, name, surname) {         
    name = datos.name;
    surname = datos.surname;
    if (name === name && surname === surname) {
        return(datos.IP)
    }
}

const datos1 = fsPromises.readFile('./links_prueba/MOCK_DATA1',).toString().split('\n');
const datos2 = fsPromises.readFile('./links_prueba/MOCK_DATA2',).toString().split('\n');
const datos3 = fsPromises.readFile('./links_prueba/MOCK_DATA3',).toString().split('\n');

Promise.all([datos1, datos2, datos3])
    .then((result) => {

        const datos = [].concat(result[0].data, result[1].data, result[2].data)
        console.log (findIPbyName( datos,'Cari','Wederell'))
    })
    
    .catch(err => {
        console.log('ERROR DESCARGANDO DATOS. NO SE PROCESAN');
    })

//-----------------------------------------------------------------------------------------------------------------------

